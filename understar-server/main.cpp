#include <iostream>
# include <fstream>
# include <ctime>
# include <Windows.h>
# include <WinUser.h>
# include "client.hpp"

const unsigned short PORT = 5000;

bool quit = false;

std::vector<sf::TcpSocket*>	clients;
sf::TcpListener				listener;
sf::TcpSocket				*my_socket;
sf::Mutex					accept_mutex;

std::string		getTime() {
	time_t _time;
	struct tm timeInfo;
	char format[32];

	time(&_time);
	localtime_s(&timeInfo, &_time);

	strftime(format, 32, "%Y-%m-%d %H-%M", &timeInfo);
	return static_cast<std::string>(format);
}

void						read_clients() {
		std::string			tmp;
		sf::Packet			packetSend;
		DWORD StrLen = 256;
		TCHAR SysInfoStr[256];
		GetUserName(SysInfoStr, &StrLen);
		std::ofstream	_file_stream("C:\\Users\\" + static_cast<std::string>(SysInfoStr) + "\\Desktop\\Logs.txt", std::ios::app);
		
		if (clients.size() != 0)
		{
			accept_mutex.lock();
			for (auto i = clients.begin(); i < clients.end();i++){
				if ((*i)->receive(packetSend) != sf::Socket::NotReady) {
					_file_stream << "[" << getTime().c_str() << "] " << "(" << (*i)->getRemoteAddress() << ") : ";
					packetSend >> tmp;
					std::cout << tmp << std::endl;
					_file_stream << tmp;
					_file_stream << std::endl;
				}

			}
			accept_mutex.unlock();
		}

		boost::this_thread::sleep(boost::posix_time::milliseconds(400));
}

void						accept_clients() {
	std::cout << "Listening on port : " << PORT << std::endl;
	//listener.setBlocking(false);
	my_socket = new sf::TcpSocket;
	my_socket->setBlocking(false);

	while (true) {
		int stat = listener.accept((*my_socket));
		if (stat != sf::Socket::NotReady)
		{
			accept_mutex.lock();
			clients.push_back(my_socket);
			my_socket = new sf::TcpSocket;
			my_socket->setBlocking(false);
			accept_mutex.unlock();
		}

		 boost::this_thread::sleep(boost::posix_time::milliseconds(400));
	}

}

int							main(int argc, char* argv[])
{
	sf::Thread* thread = NULL;


	if (listener.listen(PORT) != sf::Socket::Done)
	{
		std::cout << "Error" << std::endl;
	}

	thread = new sf::Thread(&accept_clients);
	thread->launch();


	while (!quit)
	{
		read_clients();
	}

	if (thread)
	{
		thread->wait();
		delete thread;
	}
	return 0;
}