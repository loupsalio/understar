#pragma once

#ifndef _CORE_HPP_
# define _CORE_HPP_

# define WIN32_LEAN_AND_MEAN

#include <iostream>
# include "Hook.hpp"
# include "client_rsx.hpp"

class Core
{
public:
	Core();
	~Core();
	void	start();

private:
	Hook			*_myHook;
};

#endif // !_CORE_HPP_

